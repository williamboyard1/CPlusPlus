// TP1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

int SommeDeDeuxEntiers(int Nbr1, int Nbr2)
{
    int Somme = Nbr1 + Nbr2;
    return Somme;
}

void InversionDesDeuxEntiers(int& Nbr1, int& Nbr2)
{
    int Tempo = Nbr2;

    Nbr2 = Nbr1;
    Nbr1 = Tempo;
}

void RemplaceAvecReference(int& Nbr1, int& Nbr2, int& Nbr3)
{
    Nbr3 = Nbr1 + Nbr2;
}

void RemplacementAvecPointeur(int* Nbr1, int* Nbr2, int* Nbr3)
{
    *Nbr3 = *Nbr1 + *Nbr2;
}

void RemplirTableau(int Tableau[], int TailleTableau)
{
    srand(time(NULL));
    for (int i = 0; i < TailleTableau; i++)
    {
        Tableau[i] = rand();
    }
}

void AffichageTableau(int Tableau[], int TailleTableau)
{
    for (int i = 0; i < TailleTableau; i++)
    {
        std::cout << Tableau[i] << std::endl;
    }
}

void TableauCroissant(int Tableau[], int TailleTableau)
{
    for (int i = 0; i < TailleTableau; i++)
    {
        for (int j = 0; j < TailleTableau-1; j++)
        {
            if (Tableau[j] > Tableau[j + 1])
            {
                InversionDesDeuxEntiers(Tableau[j],Tableau[j + 1]);
            }
        }
    }
}


int main()
{
    int A = 10;
    int B = 11;
    int C = 12;
    std::cout << "Valeur au depart de A : " << A << " et de B : " << B << std::endl;
    int Somme = SommeDeDeuxEntiers(A, B);
    std::cout << "Somme des deux entiers :" << Somme << std::endl;
    InversionDesDeuxEntiers(A, B);
    std::cout << "Inversion de A : " << A << " et de B : " << B << std::endl;
    RemplaceAvecReference(A, B, C);
    std::cout << "Remplacement avec reference : " << A << " " << B << " " << C << std::endl;
    RemplacementAvecPointeur(&A, &B, &C);
    std::cout << "Remplacement avec pointeur : " << A << " " << B << " " << C << std::endl;

    int Tableau[5];
    RemplirTableau(Tableau, 5);
    std::cout << "Tableau : " << std::endl;
    AffichageTableau(Tableau, 5);
    TableauCroissant(Tableau, 5);
    std::cout << "Tableau Trier : " << std::endl;
    AffichageTableau(Tableau, 5);
}

