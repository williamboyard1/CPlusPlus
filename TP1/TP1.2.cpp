// TP1.2.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>

void AjoutPoint(int &NbrPoint, int &CmptP)
{
	if (NbrPoint == 0)
	{
		NbrPoint = NbrPoint + 15;
		CmptP = CmptP +1;
	}
	else if (NbrPoint == 15)
	{
		NbrPoint = NbrPoint + 15;
		CmptP = CmptP + 1;
	}
	else if (NbrPoint == 30)
	{
		NbrPoint = NbrPoint + 10;
		CmptP = CmptP + 1;
	}
	else if (NbrPoint == 40)
	{
		CmptP = CmptP + 1;
	}
}


void AffichageScore(int CmptP1, int CmptP2, int NbrPointP1, int NbrPointP2,bool &BoolVictoire)
{
	if (CmptP1 < 4 && CmptP2 < 4)
	{
		std::cout << "P1 = " << NbrPointP1 << " - P2 = " << NbrPointP2 << std::endl;
	}
	else 
	{
		if (CmptP1 - CmptP2 >= 2) {
			std::cout << "Joueur 1 gagne" << std::endl;
			BoolVictoire = true;
		}
		else if (CmptP2 - CmptP1 >= 2) {
			std::cout << "Joueur 2 gagne" << std::endl;
			BoolVictoire = true;
		}
		else if(CmptP1 - CmptP2 == 1) 
		{
			std::cout << "Joueur 1 Avantage " << std::endl;
		}
		else if (CmptP2 - CmptP1 == 1) {
			std::cout << "Joueur 2 Avantage " << std::endl;
		}
		else if (CmptP1 == CmptP2)
		{
			std::cout << "Egalite " << std::endl;
		}
	}
}


int main()
{
	int NbrPointP1 = 0, NbrPointP2 = 0;
	int CmptP1 = 0, CmptP2 = 0;
	std::string PlayerGagnant;
	bool BoolVictoire = false;

	do
	{
		do {
			std::cout << "Quel joueur a gagne le point ? (P1 ou P2) ";
			std::cin >> PlayerGagnant;
		} while (PlayerGagnant != "P1" && PlayerGagnant != "P2");

		if (PlayerGagnant == "P1")
		{
			AjoutPoint(NbrPointP1,CmptP1);
		}
		if (PlayerGagnant == "P2")
		{
			AjoutPoint(NbrPointP2,CmptP2);
		}
		AffichageScore(CmptP1, CmptP2, NbrPointP1, NbrPointP2, BoolVictoire);
	} while (BoolVictoire == false);
}

