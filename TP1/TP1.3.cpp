// TP1.3.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//
#include <iostream>
#include <string>

void NomEnMajuscule(std::string& string)
{
    for (int i = 0; i < string.length(); i++)
    {
        string[i] = toupper(string[i]);
    }
}

int main()
{

    //Partie 1
    std::string Prenom;
    std::string Nom;

    std::cout << "Bonjour ! Quel est votre nom et prenom ?" << std::endl;

    std::cin >> Nom >> Prenom;
    NomEnMajuscule(Nom);

    std::cout << "Bonjour, " << Prenom << " " << Nom << std::endl;


    //Partie 2
    bool ConditionVictoire = false;
    srand(time(NULL));
    int ChiffreGenere= rand() % 1000 ;
    int ChiffreSaisie;
    int CompteurDeSaisie=0;

    std::cout << "Jouons a un petit jeu, Trouvez le chiffre entre 0 et 100";
    
    
    do
    {
        std::cout << "Chiffre ?";
        std::cin >> ChiffreSaisie;

        if (ChiffreSaisie > ChiffreGenere)
        {
            std::cout << "Trop grand !" << std::endl;
            CompteurDeSaisie++;
        }
        else if (ChiffreSaisie < ChiffreGenere)
        {
            std::cout << "Trop petit !" << std::endl;
            CompteurDeSaisie++;
        }
        else
        {
            std::cout << "Félicitation ! Vous avez trouver en " << CompteurDeSaisie << std::endl;
            ConditionVictoire = true;
        }
    } while (ConditionVictoire==false);
}
