#include "MorpionGame.h"

string ChoixJeuRobot = "1";
string ChoixJeu2Joueurs = "2";

string DemandeChoixDuJeu()
{
    string ChoixModeDeJeux;
    bool MenuChoixVerification = false;
    std::cout << "=======================================" << std::endl;
    std::cout << "Morpion : " << std::endl;
    std::cout << "=======================================" << std::endl;
    std::cout << "Si vous voulez jouer contre un robot, tapez 1" << std::endl;
    std::cout << "Si vous voulez jouer contre un autre joueur, tapez 2" << std::endl;
    do {
        std::cin >> ChoixModeDeJeux;
        if (ChoixModeDeJeux == ChoixJeuRobot || ChoixModeDeJeux == ChoixJeu2Joueurs)
        {
            MenuChoixVerification = true;
        }
        else
        {
            MenuChoixVerification = false;
            std::cout << "Veuillez faire une bonne saisie" << std::endl;
        }
    } while (MenuChoixVerification == false);
    return ChoixModeDeJeux;
}

void MorpionGame::Deroulement() const
{
    system("CLS");
    bool EnregistrementVictory = false;
    bool Egalite = false;
    string Robot = DemandeChoixDuJeu();
    Grille grille(3, 3);
    Morpion MorpionGame(grille);
    grille = MorpionGame.Initialisation(grille);

    std::cout << std::endl;
    if (Robot == ChoixJeuRobot)
    {
        do
        {
            //Tour J1
            MorpionGame.Afficher(grille);
            std::cout << std::endl << "Tour du joueur : " << Player1.GetNomDuJoueur() << "(" << Player1.GetPionDuJoueur() << ")" << std::endl;
            grille = MorpionGame.Remplir(Player1, grille);
            MorpionGame.Afficher(grille);
            EnregistrementVictory = MorpionGame.Victoire(grille);
            if (EnregistrementVictory == false)
                Egalite = MorpionGame.Egalite(grille);
            if (!EnregistrementVictory && !Egalite)
            {
                std::cout << std::endl << "Tour de l'ordinateur : " << std::endl;
                grille = MorpionGame.RemplirBot(grille);
                EnregistrementVictory = MorpionGame.Victoire(grille);
                if (EnregistrementVictory == false)
                    Egalite = MorpionGame.Egalite(grille);
            }
            if (EnregistrementVictory == true || Egalite == true)
            {
                std::cout << "Voulez-vous recommencer ? (1 Pour recommencer | 2 Pour quitter)" << std::endl;
                int retry;
                std::cin >> retry;
                if (retry == 1)
                {
                    grille = MorpionGame.Initialisation(grille);
                    continue;
                }
                else
                    break;
            }
        } while (true);
        system("CLS");
    }
    else if (Robot == ChoixJeu2Joueurs)
    {
        do
        {
            MorpionGame.Afficher(grille);
            std::cout << std::endl << "Tour du joueur : " << Player1.GetNomDuJoueur() << "(" << Player1.GetPionDuJoueur() << ")" << std::endl;
            //Tour J1
            grille = MorpionGame.Remplir(Player1, grille);
            MorpionGame.Afficher(grille);
            EnregistrementVictory = MorpionGame.Victoire(grille);
            if (EnregistrementVictory == false)
                Egalite = MorpionGame.Egalite(grille);
            //Tour J2
            if (!EnregistrementVictory && !Egalite)
            {
                std::cout << std::endl << "Tour du joueur : " << Player2.GetNomDuJoueur() << "(" << Player2.GetPionDuJoueur() << ")" << std::endl;
                grille = MorpionGame.Remplir(Player2, grille);
                EnregistrementVictory = MorpionGame.Victoire(grille);
                if (EnregistrementVictory == false)
                    Egalite = MorpionGame.Egalite(grille);
            }
            //Restart une partie
            if (EnregistrementVictory == true || Egalite == true)
            {
                std::cout << "Voulez-vous recommencer ? (1 Pour recommencer | 2 Pour quitter)" << std::endl;
                int retry;
                std::cin >> retry;
                if (retry == 1)
                {
                    grille = MorpionGame.Initialisation(grille);
                    continue;
                }
                else
                    break;
            }
        } while (true);
        system("CLS");
    }
}

MorpionGame::MorpionGame(Player P1, Player P2)
{
    Player1 = P1;
    Player2 = P2;

}
