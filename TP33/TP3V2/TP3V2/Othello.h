#pragma once
#include <iostream>
#include "Player.h"
#include "Grille.h"

class Othello
{
public:
    //Constructeur 
    Othello(Grille grille);
    Othello();

    Grille Remplir(Player P, Player adversaire, Grille grille1);

    void Afficher(Grille grille) const;
    Grille Initialisation(Grille grille) const;

    bool CanFlipPieces(Grille board, int row, int col, Player player) const;
    bool CanBePlace(Grille, int row, int col) const;
    bool CanFlipPiecesInDirection(Grille board, int row, int col, int drow, int dcol, Player player) const;

    Grille flipDiscsInDirection(Grille board, int row, int col, Player player, Player adversaire, int dx, int dy);

    Grille flipDiscs(Grille board, int row, int col, Player player, Player adversaire);





private:

    Grille grille;

};