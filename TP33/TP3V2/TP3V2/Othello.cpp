#include <iostream>
#include "Othello.h"

Othello::Othello(Grille grille) {
	this->grille = grille;
}

Othello::Othello() {

}

Grille Othello::Remplir(Player P, Player adversaire, Grille grille1) {
	bool Remplis = false;
	bool Saisie = false;
	int Ligne = 0, Colonne = 0;
	do
	{
		do
		{
			std::cout << "Ligne ?";
			std::cin >> Ligne;
			std::cout << "Colonne ?";
			std::cin >> Colonne;
			if (Ligne >= 1 && Ligne <= grille1.getNbRows() && Colonne >= 1 && Colonne <= grille1.getNbColumns())
				Saisie = true;
			else
			{
				std::cout << "Erreur lors de la saisie, le nombre doit etre entre 1 et " << grille1.getNbColumns() << std::endl;
				Saisie = false;
			}
		} while (Saisie == false);
		Colonne = Colonne - 1;
		Ligne = Ligne - 1;
		std::cout << "Afficher valeurs de la case :" << grille.grid[Ligne][Colonne];
		if (grille.grid[Ligne][Colonne] != '-')
		{
			std::cout << "L'emplacement a deja ete pris !" << std::endl;
			Remplis = false;
		}
		else
		{
			grille.grid[Ligne][Colonne] = P.GetPionDuJoueur();
			Remplis = true;
		}
	} while (Remplis == false);

	return grille1;
}

void Othello::Afficher(Grille grille) const {
	for (int i = 0; i < grille.getNbRows(); i++) {
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			std::cout << grille.grid[i][j];

			if (j == grille.getNbColumns() - 1) {
				std::cout << std::endl;
			}
			else {
				std::cout << " | ";
			}
		}
	}
}


Grille Othello::Initialisation(Grille grille) const
{
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			grille.grid[i][j] = 45;
		}
	}
	grille.grid[3][3] = 'X';
	grille.grid[4][4] = 'X';
	grille.grid[4][3] = 'O';
	grille.grid[3][4] = 'O';
	return grille;
}



bool Othello::CanFlipPieces(Grille board, int row, int col, Player player) const {


	// V�rifie si la position est vide
	if (board.grid[row][col] != '-') {
		return false;
	}



	// Parcourt chaque direction � partir de la position donn�e
	for (int drow = -1; drow <= 1; drow++) {
		for (int dcol = -1; dcol <= 1; dcol++) {
			if (drow == 0 && dcol == 0) continue;  // Ignore la direction "immobile"
			if (CanFlipPiecesInDirection(board, row, col, drow, dcol, player)) {
				return true;
			}
		}
	}
	return false;
}

bool Othello::CanBePlace(Grille, int row, int col) const
{
	int cmpt = 0;
	if (row > 1 && row < 8 && col > 1 && col < 8)
	{
		if (grille.grid[row - 1][col - 1] != '-')
		{
			std::cout << "TOP-LEFT" << endl;
			cmpt++;
		}
		if (grille.grid[row - 1][col] == '-')
		{
			std::cout << "TOP" << endl;
			cmpt++;
		}
		if (grille.grid[row - 1][col + 1] == '-')
		{
			std::cout << "TOP-RIGHT" << endl;
			cmpt++;
		}		
		if (grille.grid[row][col - 1] == '-')
		{
			std::cout << "LEFT" << endl;
			cmpt++;
		}
		if (grille.grid[row][col + 1] == '-')
		{
			std::cout << "RIGHT" << endl;
			cmpt++;
		}
		if (grille.grid[row + 1][col - 1] == '-')
		{
			std::cout << "BOTTOM-LEFT" << endl;
			cmpt++;
		}
		if (grille.grid[row + 1][col] == '-')
		{
			std::cout << "BOTTOM" << endl;
			cmpt++;
		}
		if (grille.grid[row + 1][col + 1] == '-')
		{
			std::cout << "BOTTOM-RIGHT" << endl;
			cmpt++;
		}
		
		if (cmpt >= 8)
		{
			return false;
		}
	}
	return true;
}

bool Othello::CanFlipPiecesInDirection(Grille board, int row, int col, int drow, int dcol, Player player) const {
	// V�rifie si la position est hors du plateau
	if (row < 0 || row >= 8 || col < 0 || col >= 8) {
		return false;
	}

	// V�rifie si la position est vide
	if (board.grid[row][col] != '-') {
		return false;
	}

	if (grille.grid[row][col] == player.GetPionDuJoueur()) {
		return false;
	}

	// V�rifie si la pi�ce suivante est de la m�me couleur que le joueur actuel
	row += drow;
	col += dcol;
	if (grille.grid[row][col] != player.GetPionDuJoueur()) {
		return false;
	}

	// V�rifie si la pi�ce suivante est de la couleur oppos�e
	row += drow;
	col += dcol;
	while (grille.grid[row][col] != player.GetPionDuJoueur()) {
		// Avance dans la direction donn�e jusqu'� trouver une pi�ce vide ou de la couleur du joueur
		row += drow;
		col += dcol;
	}
}

Grille Othello::flipDiscs(Grille board, int row, int col, Player player, Player adversaire) {
	// Flip discs in all 8 directions
	board = flipDiscsInDirection(board, row, col, player, adversaire, -1, -1); // top-left
	board = flipDiscsInDirection(board, row, col, player, adversaire, -1, 0); // top
	board = flipDiscsInDirection(board, row, col, player, adversaire, -1, 1); // top-right
	board = flipDiscsInDirection(board, row, col, player, adversaire, 0, -1); // left
	board = flipDiscsInDirection(board, row, col, player, adversaire, 0, 1); // right
	board = flipDiscsInDirection(board, row, col, player, adversaire, 1, -1); // bottom-left
	board = flipDiscsInDirection(board, row, col, player, adversaire, 1, 0); // bottom
	board = flipDiscsInDirection(board, row, col, player, adversaire, 1, 1); // bottom-right

	return board;
}
//
// Flips the discs in the given direction starting from the given position
Grille Othello::flipDiscsInDirection(Grille board, int row, int col, Player player, Player adversaire, int dx, int dy) {

	int x = row + dx;
	int y = col + dy;




	//Go through the grid until we reach an empty cell or the edge of the board
	while (x >= 0 && x < 8 && y >= 0 && y < 8 && (board.grid[x][y] != 0 && board.grid[x][y] == player.GetPionDuJoueur())) {
		board.grid[x][y] = adversaire.GetPionDuJoueur();
		x += dx;
		y += dy;
	}

	//If the traversel ended on an empty cell , it means that no discs can be flipped.
	//Otherwise , if it ended on a disc belonging to the player who placed the disc, flip all the discs that were encountrerd along the way
	//if (x >= 0 && x < 8 && y >= 0 && y < 8 && board.grid[x][y] == player.GetPionDuJoueur()) {
	//	for (Point p : flipped) {
	//		board.grid[p.x][p.y] = player.GetPionDuJoueur();
	//	}
	//}

	return board;
}








