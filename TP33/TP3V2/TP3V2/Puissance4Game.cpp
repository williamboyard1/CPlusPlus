#include "Puissance4Game.h"


void Puissance4Game::Deroulement() const
{
    bool EnregistrementVictory = false;
    bool EnregistrementEgalite = false;
    int ConditionVictoire = 4;
    Grille grille(7, 4);
    Puissance4 Puissance4Game(grille);
    grille = Puissance4Game.Initialisation(grille);


    system("CLS");
    std::cout << "=======================================" << std::endl;
    std::cout << "Puissance 4 : " << std::endl;
    std::cout << "=======================================" << std::endl;
    do {
        Puissance4Game.Afficher(grille);
        std::cout << std::endl << "Tour du joueur : " << Player1.GetNomDuJoueur() << "(" << Player1.GetPionDuJoueur() << ")" << std::endl;
        grille = Puissance4Game.Remplir(Player1, grille);
        Puissance4Game.Afficher(grille);
        EnregistrementVictory = Puissance4Game.Victoire(grille, ConditionVictoire);
        EnregistrementEgalite = Puissance4Game.Egalite(grille);
        if (EnregistrementVictory == false && EnregistrementEgalite == false)
        {
            std::cout << std::endl << "Tour du joueur : " << Player2.GetNomDuJoueur() << "(" << Player2.GetPionDuJoueur() << ")" << std::endl;
            grille = Puissance4Game.Remplir(Player2, grille);
            EnregistrementVictory = Puissance4Game.Victoire(grille, ConditionVictoire);
            EnregistrementEgalite = Puissance4Game.Egalite(grille);
        }

        if (EnregistrementVictory == true || EnregistrementEgalite == true)
        {
            std::cout << "Voulez-vous recommencer ? (1 Pour recommencer | 2 Pour quitter)" << std::endl;
            int retry;
            std::cin >> retry;
            if (retry == 1)
            {
                grille = Puissance4Game.Initialisation(grille);
                continue;
            }
            else
                break;
        }
    } while (true);
    system("CLS");
}

Puissance4Game::Puissance4Game(Player P1, Player P2)
{
    Player1 = P1;
    Player2 = P2;

}

