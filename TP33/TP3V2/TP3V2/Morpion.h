#pragma once
#include <iostream>
#include "Player.h"
#include "Grille.h"

class Morpion
{
public:
	//Constructeur 
	Morpion(Grille grille);
	Morpion();

	Grille Remplir(Player P, Grille grille) const;
	Grille RemplirBot(Grille grille) const;
	void Afficher(Grille grille) const;
	Grille Initialisation(Grille grille) const;
	bool Victoire(Grille grille) const;
	bool Egalite(Grille grille) const;

private:

	Grille grille;

};
