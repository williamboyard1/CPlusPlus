#include "OthelloGame.h"

void OthelloGame::Deroulement() const
{
    bool EnregistrementVictory = false;
    bool Egalite = false;
    int Robot = 0;
    int ConditionVictoire = 3;
    Grille grille(8, 8);
    Othello OthelloGame(grille);
    grille = OthelloGame.Initialisation(grille);

    bool Saisie = false;
    int Ligne, Colonne;
    system("CLS");
    std::cout << "=======================================" << std::endl;
    std::cout << "Othello : " << std::endl;
    std::cout << "=======================================" << std::endl;
    std::cout << std::endl;
    do
    {
        OthelloGame.Afficher(grille);
        std::cout << std::endl << "Tour du joueur : " << Player1.GetNomDuJoueur() << "(" << Player1.GetPionDuJoueur() << ")" << std::endl;
        //Tour J1
        Ligne = 0;
        Colonne = 0;
        grille = OthelloGame.Remplir(Player1, Player2, grille);
        grille = OthelloGame.flipDiscs(grille, Ligne, Colonne, Player1, Player2);

        //Tour J2
        OthelloGame.Afficher(grille);
        std::cout << std::endl << "Tour du joueur : " << Player2.GetNomDuJoueur() << "(" << Player2.GetPionDuJoueur() << ")" << std::endl;
        grille = OthelloGame.Remplir(Player2, Player1, grille);
        grille = OthelloGame.flipDiscs(grille, Ligne, Colonne, Player2, Player1);
    } while (true);
    system("CLS");
}


OthelloGame::OthelloGame(Player P1, Player P2)
{
    Player1 = P1;
    Player2 = P2;

}