#include "Morpion.h"
#include <regex>


Morpion::Morpion(Grille grille)
{
	this->grille = grille;
}

Morpion::Morpion()
{

}


Grille Morpion::Remplir(Player P, Grille grille) const
{
	bool Remplis = false;
	bool Saisie = false;
	int Ligne = 0, Colonne = 0;
	do
	{
		do
		{
			std::cout << "Ligne ?";
			std::cin >> Ligne;
			std::cout << "Colonne ?";
			std::cin >> Colonne;
			if (Ligne >= 1 && Ligne <= 3 && Colonne >= 1 && Colonne <= 3)
				Saisie = true;
			else
				std::cout << "Erreur lors de la saisie, le nombre doit etre entre 1 et 3" << std::endl;
		} while (Saisie == false);
		Colonne = Colonne - 1;
		Ligne = Ligne - 1;
		if (grille.grid[Ligne][Colonne] != '-')
		{
			std::cout << "L'emplacement a deja ete pris !" << std::endl;
			Remplis = false;
		}
		else
		{
			grille.grid[Ligne][Colonne] = P.GetPionDuJoueur();
			Remplis = true;
		}
	} while (Remplis == false);
	return grille;
}

Grille Morpion::RemplirBot(Grille grille) const
{
	bool Remplis = false;
	int RandomColumn;
	int RandomRow;

	do
	{
		RandomColumn = (rand() % 3);
		RandomRow = (rand() % 3);
		if (grille.grid[RandomColumn][RandomRow] != '-')
		{
			std::cout << "L'ordinateur est pas doue ! " << std::endl;
		}
		else if (grille.grid[RandomColumn][RandomRow] == '-')
		{
			std::cout << "L'ordinateur a remplis une case" << std::endl;
			grille.grid[RandomColumn][RandomRow] = '0';
			Remplis = true;
		}
	} while (Remplis == false);

	return grille;
}

void Morpion::Afficher(Grille grille) const
{
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			std::cout << grille.grid[i][j];

			if (j == grille.getNbColumns() - 1)
			{
				std::cout << std::endl;
			}
			else
			{
				std::cout << " | ";
			}
		}
	}
}

Grille Morpion::Initialisation(Grille grille) const
{
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			grille.grid[i][j] = '-';
		}
	}
	return grille;
}

bool Morpion::Victoire(Grille grille) const
{
	for (int i = 0; i < 3; i++)
	{
		if (grille.grid[i][0] == grille.grid[i][1] && grille.grid[i][1] == grille.grid[i][2] && grille.grid[i][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (grille.grid[0][i] == grille.grid[1][i] && grille.grid[1][i] == grille.grid[2][i] && grille.grid[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (grille.grid[0][0] == grille.grid[1][1] && grille.grid[1][1] == grille.grid[2][2] && grille.grid[0][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (grille.grid[2][0] == grille.grid[1][1] && grille.grid[1][1] == grille.grid[0][2] && grille.grid[2][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else
			return false;
	}
}

bool Morpion::Egalite(Grille grille) const
{
	// Egalit�
	int compteur = 0;
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			if (grille.grid[i][j] != '-')
			{
				compteur++;
			}
		}
	}
	if (compteur == grille.getNbColumns() * grille.getNbRows())
	{
		cout << "Egalite" << std::endl;
		return true;
	}
	return false;
}




