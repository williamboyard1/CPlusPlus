#include "Player.h"

Player::Player(char PionDuJoueur, std::string NomDuJoueur)
{
    this->PionDuJoueur = PionDuJoueur;
    this->NomDuJoueur = NomDuJoueur;
}

Player::Player() {}

char Player::GetOppositePion() const {
    if (PionDuJoueur == 'X') {
        return 'O';
    }
    else {
        return 'X';
    }
}