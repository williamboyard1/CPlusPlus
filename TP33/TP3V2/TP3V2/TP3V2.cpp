#include <iostream>
#include <windows.h>
#include "Player.h"
#include "Morpion.h"
#include "MorpionGame.h"
#include "Puissance4Game.h"
#include "Puissance4.h"
#include "Othello.h"
#include "OthelloGame.h"
#include "Grille.h"

string ChoixMorpion = "1";
string ChoixPuissance4 = "2";
string ChoixOthello = "3";
const Player Player1{ 'X',"Joueur 1" };
const Player Player2{ 'O',"Joueur 2" };

string DemandeUtilisateurLeChoixDuJeu() {
    string MenuChoix;
    bool MenuChoixVerification = false;
    do {
        //Menu 
        std::cout << "MENU :" << std::endl;
        std::cout << ChoixMorpion << " - Morpion " << std::endl;
        std::cout << ChoixPuissance4 << " - Puissance 4" << std::endl;
        std::cout << ChoixOthello << " - Othello" << std::endl;
        std::cin >> MenuChoix;
        if (MenuChoix == ChoixMorpion || MenuChoix == ChoixPuissance4 || MenuChoix == ChoixOthello)
        {
            MenuChoixVerification = true;
            std::cout << "La saisie est bonne" << std::endl;
        }
        else
        {
            MenuChoixVerification = false;
            std::cout << "Veuillez faire une bonne saisie" << std::endl;
        }
    } while (MenuChoixVerification == false);

    return MenuChoix;
}
void ExecutionChoixDuJeu(string MenuChoix)
{
    do
    {
        if (MenuChoix == ChoixMorpion)
        {
            MorpionGame JeuxDeMorpion(Player1, Player2);
            JeuxDeMorpion.Deroulement();
        }
        else if (MenuChoix == ChoixPuissance4)
        {
            Puissance4Game JeuxDePuissance4(Player1, Player2);
            JeuxDePuissance4.Deroulement();
        }
        else if (MenuChoix == ChoixOthello)
        {
            std::cout << "Le jeu n'est pas encore installer" << std::endl << std::endl;
            OthelloGame Othello(Player1, Player2);
            Othello.Deroulement();
        }
    } while (true);
}

int main()
{
    ExecutionChoixDuJeu(DemandeUtilisateurLeChoixDuJeu());
}



