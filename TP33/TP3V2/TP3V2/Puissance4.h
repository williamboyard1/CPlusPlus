#pragma once
#include <iostream>
#include "Player.h"
#include "Grille.h"

class Puissance4 
{
public:

	Puissance4(Grille grille);
	Grille Remplir(Player P, Grille grille) const;
	Puissance4();
	void Afficher(Grille grille);
	Grille Initialisation(Grille grille);
	bool Victoire(Grille grille, int ConditionVictoire) const;
	bool Egalite(Grille grille) const;

private:

	Grille grille;
};
