#pragma once
#include "Othello.h"
#include "Player.h"

class OthelloGame : public Othello
{
public:
    void Deroulement() const;
    OthelloGame(Player P1, Player P2);

    Player getPlayer1() { return Player1; }
    Player getPlayer2() { return Player2; }
private:
    Player Player1;
    Player Player2;
};