#include "Puissance4.h"



Puissance4::Puissance4()
{
}

Puissance4::Puissance4(Grille grille) {
	this->grille = grille;
}

Grille Puissance4::Remplir(Player P, Grille grille) const
{
	bool Remplis = false;
	bool Saisie = false;
	int Colonne;
	do
	{
		do
		{
			std::cout << "Colonne ?";
			std::cin >> Colonne;
			if (Colonne >= 1 && Colonne <= 7)
				Saisie = true;
			else
				std::cout << "Erreur lors de la saisie, le nombre doit etre entre 1 et 7" << std::endl;
		} while (Saisie == false);
		Colonne = Colonne - 1;
		if (grille.grid[0][Colonne] != '-')
		{
			std::cout << "Colonne complete !" << std::endl;
			Remplis = false;
		}
		else
		{
			for (int Ligne = 3; Ligne >= 0; Ligne--)
			{
				if (grille.grid[Ligne][Colonne] == '-' && Remplis == false)
				{
					grille.grid[Ligne][Colonne] = P.GetPionDuJoueur();
					Remplis = true;
				}
			}
		}
	} while (Remplis == false);

	return grille;
}

void Puissance4::Afficher(Grille grille)
{
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			std::cout << grille.grid[i][j];
			if (j == grille.getNbColumns() - 1)
			{
				std::cout << std::endl;
			}
			else
			{
				std::cout << " | ";
			}
		}
	}

}

Grille Puissance4::Initialisation(Grille grille)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			grille.grid[i][j] = '-';
		}
	}


	return grille;
}

bool Puissance4::Victoire(Grille grille, int ConditionVictoire) const
{
	const int NombredePionPourVictoire = 4;
	//Lignes
	for (int i = 0; i < NombredePionPourVictoire; i++)
	{
		for (int j = 0; j < NombredePionPourVictoire; j++)
		{
			if (grille.grid[i][j] == grille.grid[i][j + 1] && grille.grid[i][j] == grille.grid[i][j + 2] && grille.grid[i][j] == grille.grid[i][j + 3] && grille.grid[i][j] != '-')
			{
				std::cout << "Victoire d'un Joueur ! " << std::endl;
				return true;
			}
		}
	}
	//Colonnes
	for (int i = 0; i < grille.getNbColumns(); i++)
	{
		if (grille.grid[0][i] == grille.grid[1][i] && grille.grid[1][i] == grille.grid[2][i] && grille.grid[2][i] == grille.grid[3][i] && grille.grid[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
	}

	//Diagonale
	for (int i = 0; i < NombredePionPourVictoire; i++)
	{
		//Du Haut vers le Bas
		if (grille.grid[0][i] == grille.grid[1][i + 1] && grille.grid[0][i] == grille.grid[2][i + 2] && grille.grid[0][i] == grille.grid[3][i + 3] && grille.grid[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		//Du Bas vers le Haut
		if (grille.grid[3][i] == grille.grid[2][i + 1] && grille.grid[3][i] == grille.grid[1][i + 2] && grille.grid[3][i] == grille.grid[0][i + 3] && grille.grid[3][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
	}
	return false;
}

bool Puissance4::Egalite(Grille grille) const
{
	// Egalit�
	int compteur = 0;
	for (int i = 0; i < grille.getNbRows(); i++)
	{
		for (int j = 0; j < grille.getNbColumns(); j++)
		{
			if (grille.grid[i][j] != '-')
			{
				compteur++;
			}
		}
	}
	if (compteur == grille.getNbColumns() * grille.getNbRows())
	{
		cout << "Egalite" << std::endl;
		return true;
	}
	return false;

}
