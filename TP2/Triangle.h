#pragma once
#include "Point.h"

#include <array>
#include <iostream>

class Triangle
{

public:
	//GET 
	inline Point GetP1() const {
		return P1;
	}
	inline Point GetP2() const {
		return P2;
	}
	inline Point GetP3() const {
		return P3;
	}
	//SET
	inline void SetPoint1(Point Point)
	{
		this->P1 = Point;
	}
	inline void SetPoint2(Point Point)
	{
		this->P2 = Point;
	}
	inline void SetPoint3(Point Point)
	{
		this->P3 = Point;
	}
	//METHODES
	float Base(Point P1, Point P2, Point P3);
	float Hauteur(Point P1, Point P2, Point P3);
	float Surface(Point P1, Point P2, Point P3);
	std::array<float, 3> Longueurs(Point P1, Point P2, Point P3);
	bool IsIsocele(Point P1, Point P2, Point P3);
	bool IsRectangle(Point P1, Point P2, Point P3);
	bool IsEquilateral(Point P1, Point P2, Point P3);
	void Afficher();
	//CONSTRUCTEUR 
	Triangle(Point P1,Point P2,Point P3)
	{
		this->P1 = P1;
		this->P2 = P2;
		this->P3 = P3;
	}
private:
	Point P1;
	Point P2;
	Point P3;
};

