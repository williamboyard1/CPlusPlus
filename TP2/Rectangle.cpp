#include "Rectangle.h"
#include <iostream>

//Methode Surface (Surface = Longueur*Largeur)
int Rectangle::Surface(int longueur, int largeur)
{
	const int ResultSurface = longueur * largeur;
	return ResultSurface;
}

//Methode Surface (Surface = Longueur*2 + Largeur*2)
int Rectangle::Perimetre(int longueur, int largeur)
{
	const int ResultPerimetre = (longueur * 2) + (largeur * 2);
	return ResultPerimetre;
}

//Methode Affichage
void Rectangle::Afficher()
{
	const int perimetre = Rectangle::Perimetre(longueur, largeur);
	const int surface = Rectangle::Surface(longueur, largeur);
	std::cout << "////////////////////////////////////////////" << std::endl;
	std::cout << "Information du rectangle :" << std::endl << std::endl;
	std::cout << "Longueur :" << longueur << std::endl;
	std::cout << "Largeur :" << largeur << std::endl;
	std::cout << "Perimetre : " << perimetre << std::endl;
	std::cout << "Surface : " << surface << std::endl;
}