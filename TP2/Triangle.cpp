#include "Triangle.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <array>
#include <iostream>


//Methode Longueurs (permets d'obtenir les longueurs grace a des coordonn�es :(X2-X1)�+(Y2-Y1)� , retourne un tableau qui contient les valeurs)
std::array<float, 3> Triangle::Longueurs(Point P1, Point P2, Point P3)
{
	const float DistanceP1P2 = sqrt(((P2.x - P1.x) * (P2.x - P1.x)) + ((P2.y - P1.y) * (P2.y - P1.y)));
	const float DistanceP2P3 = sqrt(((P3.x - P2.x) * (P3.x - P2.x)) + ((P3.y - P2.y) * (P3.y - P2.y)));
	const float DistanceP3P1 = sqrt(((P1.x - P3.x) * (P1.x - P3.x)) + ((P1.y - P3.y) * (P1.y - P3.y)));
	return { DistanceP1P2,DistanceP2P3,DistanceP3P1 };
}

//Methode Surface ( Surface = (Cot�1 + Cot�2 + Cot�3)/2  )
float Triangle::Surface(Point P1, Point P2, Point P3)
{
	const std::array<float, 3> TableauDeLongueurs = Triangle::Longueurs(P1, P2, P3);
	float ValueSurface = 0;
	for (int i = 0; i < 3; i++)
	{
		ValueSurface = ValueSurface + TableauDeLongueurs[i];
	}
	ValueSurface = ValueSurface / 2;
	return ValueSurface;
}

//Methode Base (compare toutes les longueurs, renvoie la plus grande)
float Triangle::Base(Point P1, Point P2, Point P3)
{
	const std::array<float, 3> TableauDeLongueurs = Triangle::Longueurs(P1, P2, P3);
	float MaxValue = 0;
	for (int i = 0; i < 3; i++)
	{
		if (MaxValue < TableauDeLongueurs[i])
			MaxValue = TableauDeLongueurs[i];
	}
	return MaxValue;
}


//Methode Hauteur (Formule Heron : Hauteur= (2*Surface)/ Base)
float Triangle::Hauteur(Point P1, Point P2, Point P3)
{
	float ValueHauteur = 0;
	const float ValueBaseTriangle = Triangle::Base(P1, P2, P3);
	const float ValueSurface = Triangle::Surface(P1, P2, P3);
	ValueHauteur = (2 * ValueSurface) / ValueBaseTriangle;
	return ValueHauteur;
}


//Methode reconnaissance du type de triangle 
//Isocele (Compare si deux cot�s ont la m�me valeur, renvoie true)
bool Triangle::IsIsocele(Point P1, Point P2, Point P3)
{
	bool Verif = false;
	const std::array<float, 3> TableauDeLongueurs = Triangle::Longueurs(P1, P2, P3);
	if (TableauDeLongueurs[0] == TableauDeLongueurs[1] || TableauDeLongueurs[0] == TableauDeLongueurs[2] || TableauDeLongueurs[1] == TableauDeLongueurs[2])
		Verif = true;
	else
		Verif = false;
	if (Triangle::IsEquilateral(P1,P2,P3) == true)
		Verif = false;
	return Verif;
}

//Rectangle (utilisation de pythagore, si le hypothenuse� = Cot�1� + Cot�, renvoie true)
bool Triangle::IsRectangle(Point P1, Point P2, Point P3)
{
	bool Verif = false;
	const std::array<float, 3> TableauDeLongueurs = Triangle::Longueurs(P1, P2, P3);
	const float MaxValue = Triangle::Base(P1, P2, P3);
	float CoteA=0;
	bool CoteAInscrit = false;
	float CoteB=0;
	for (int i = 0; i < 3; i++)
	{
		if (MaxValue > TableauDeLongueurs[i] && CoteAInscrit==false)
		{
			CoteA = TableauDeLongueurs[i];
			CoteAInscrit = true;
		}
		else if (MaxValue > TableauDeLongueurs[i] && CoteAInscrit == true)
		{
			CoteB = TableauDeLongueurs[i];
		}
	}

	if ((MaxValue * MaxValue) == ((CoteA * CoteA) + (CoteB * CoteB)))
		Verif = true;
	else
		Verif = false;


	return Verif;
}

//Equilateral (Compare les 3 cot�s du triangles, si ils sont egaux, renvoie true)
bool Triangle::IsEquilateral(Point P1, Point P2, Point P3)
{
	bool Verif=false;
	const std::array<float, 3> TableauDeLongueurs = Triangle::Longueurs(P1, P2, P3);
	if (TableauDeLongueurs[0] == TableauDeLongueurs[1] && TableauDeLongueurs[0] == TableauDeLongueurs[2])
		Verif = true;
	return Verif;
}


//Fonction Affichage
void Triangle::Afficher()
{
	const bool equilateral = Triangle::IsEquilateral(P1, P2, P3);
	const bool rectangle = Triangle::IsRectangle(P1, P2, P3);
	const bool isocele = Triangle::IsIsocele(P1, P2, P3);
	const float base = Triangle::Base(P1, P2, P3);
	const float hauteur = Triangle::Hauteur(P1, P2, P3);
	const float surface = Triangle::Surface(P1, P2, P3);
	const std::array<float,3> TabLongueurs = Triangle::Longueurs(P1, P2, P3);


	std::cout << "////////////////////////////////////////////" << std::endl;
	std::cout << "Information du triangle :" << std::endl << std::endl;
	std::cout << "Base :" << base << std::endl;
	std::cout << "Hauteur :" << hauteur << std::endl;
	std::cout << "Surface :" << surface << std::endl;
	for (int i = 0; i < 3; i++)
	{
		std::cout << "Longueur " << i << " :" << TabLongueurs[i] << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Equilateral :" << equilateral << std::endl;
	std::cout << "Rectangle :" << rectangle << std::endl;
	std::cout << "Isocele :" << isocele << std::endl;

}
