#pragma once
#include "Point.h"

class Rectangle
{

public :
	//GET
	inline int GetLongeur() const 
	{
          return longueur;
    }
	inline int GetLargeur() const 
	{
		return largeur;
	}
	inline Point GetPoint() const 
	{
		return superieurGauche;
	}
	//SET
	inline void SetLongeur(int Longueur)
	{
		this->longueur = Longueur;
	}
	inline void SetLargeur(int Largeur)
	{
		this->largeur = Largeur;
	}
	inline void SetPoint(Point Point)
	{
		this->superieurGauche = Point;
	}
	//CONSTRUCTEUR
	Rectangle(Point point, int longueur, int largeur) {
		this->superieurGauche = point;
		this->longueur = longueur;
		this->largeur = largeur;
	}
	//METHODES
	int Surface(int longueur, int largeur);
	int Perimetre(int longueur, int largeur);
	void Afficher();
private:
	int longueur;
	int largeur;
	Point superieurGauche;

};

