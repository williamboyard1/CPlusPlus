// TP2.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.


#include <iostream>
#include "Rectangle.h"
#include "Point.h"
#include "Cercle.h"
#include "Triangle.h"
int main()
{
    //Point 
    const Point P1{0,0};

    //Rectangle
    Rectangle R1 = Rectangle::Rectangle(P1, 5, 5);
    R1.Afficher();

    //Cercle
    const Point P2{ 2.5,2.5 };
    const Point P3{ 0,5 };
    Cercle C1 = Cercle::Cercle(P1, 10);
    C1.Afficher();
    std::cout << "Point [" << P3.x << "; " << P3.y << "] dans le cercle :" << C1.PointInCercle(P3) << std::endl;
    std::cout << "Point [" << P3.x << "; " << P3.y << "] sur le cercle :" << C1.PointOnCercle(P3) << std::endl;
    std::cout << "Point ["<< P2.x << "; " << P2.y << "] dans le cercle :" << C1.PointInCercle(P2) << std::endl;
    std::cout << "Point [" << P2.x << "; " << P2.y << "] sur le cercle :" << C1.PointOnCercle(P2) << std::endl;



    //Triangle
    //Equilateral
    const Point P4{ 0,0 };
    const Point P5{ 2,0 };
    const Point P6{ 1, sqrt(3) };
    Triangle T1 = Triangle::Triangle(P4, P5, P6);
    T1.Afficher();
    //Isocel 
    const Point P7{0,0};
    const Point P8{ -16,0 };
    const Point P9{ -8,14 };
    Triangle T2 = Triangle::Triangle(P7,P8,P9); 
    T2.Afficher();
    //Rectangle()
    const Point P10{ 0,0 };
    const Point P11{ 4,0 };
    const Point P12{ 0,3 };
    Triangle T3 = Triangle::Triangle(P10,P11,P12);
    T3.Afficher();
}

