﻿#include "Cercle.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

//Methode surface (π*R²)
float Cercle::Surface(float diametre)
{
	const float rayon = (diametre / 2);
	const float ResultSurface = (rayon*rayon)*M_PI;
	return ResultSurface;
}

//Methode perimetre (Rayon*2*π) 
float Cercle::Perimetre(float diametre)
{
	const float rayon = (diametre / 2);
	const float ResultPerimetre = rayon*2*M_PI;
	return ResultPerimetre;
}

//Methode Point sur un cercle
//Calcul de rayon entre le centre et le point passer en parametre : (X2-X1)²+(Y2-Y1)²
//Puis comparaison avec la taille du rayon, si =, renvoie true
bool Cercle::PointOnCercle(Point point)
{
	bool verif = false;
	const float rayon = diametre / 2;
	float Xvalue = (point.x - centreCercle.x)*(point.x - centreCercle.x);
	float Yvalue = (point.y - centreCercle.y)*(point.y - centreCercle.y);
	float DistanceEntreCentrePoint = sqrt(Xvalue + Yvalue);
	
	if (DistanceEntreCentrePoint == rayon)
		verif = true;
	else
		verif = false;

	return verif;
}

//Methode Point dans un cercle
//Calcul de rayon entre le centre et le point passer en parametre : (X2-X1)²+(Y2-Y1)²
//Puis comparaison avec la taille du rayon, si <, renvoie true
bool Cercle::PointInCercle(Point point)
{
	bool verif = false;
	const float rayon = diametre / 2;
	float Xvalue = (point.x - centreCercle.x) * (point.x - centreCercle.x);
	float Yvalue = (point.y - centreCercle.y) * (point.y - centreCercle.y);
	float DistanceEntreCentrePoint = sqrt(Xvalue + Yvalue);

	if (DistanceEntreCentrePoint < rayon)
		verif = true;
	else
		verif = false;

	return verif;
}

//Methode Affichage
void Cercle::Afficher()
{
	const float surface = Cercle::Surface(diametre);
	const float perimetre = Cercle::Perimetre(diametre);
	std::cout << "////////////////////////////////////////////" << std::endl;
	std::cout << "Information du cercle :" << std::endl << std::endl;
	std::cout << "Diametre:" << diametre << std::endl;
	std::cout << "Centre du cercle : [" << centreCercle.x << ";" << centreCercle.y << "]" << std::endl;
	std::cout << "Surface=" << surface << std::endl;
	std::cout << "Perimetre=" << perimetre << std::endl;
}