﻿#pragma once
#include "Point.h"
class Cercle
{
public:

	
	//GET
	inline int GetDiametre() const {
		return diametre;
	}
	inline Point GetPoint() const {
		return centreCercle;
	}
	//SET
	inline void SetLargeur(float Diametre)
	{
		this->diametre = Diametre;
	}
	inline void SetPoint(Point Point)
	{
		this->centreCercle = Point;
	}
	//METHODES
	float Perimetre(float diametre);
	float Surface(float diametre);
	bool PointOnCercle(Point point);
	bool PointInCercle(Point point);
	void Afficher();

	//CONSTRUCTEUR
	Cercle(Point point, float diametre) {
		this->centreCercle = point;
		this->diametre = diametre;
	}
private:
	Point centreCercle;
	float diametre;
};



