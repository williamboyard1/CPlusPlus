#include "Morpion.h"

void Morpion::Afficher()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << GrilleMorpion[i][j];
		}
		std::cout << std::endl;
	}
}

void Morpion::Initialisation()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			GrilleMorpion[i][j] = '-';
		}
	}
}

void Morpion::Remplir(Player P)
{
	bool Remplis=false;
	bool Saisie = false;
	int Ligne, Colonne;
	do
	{
		do
		{
			std::cout << "Ligne ?";
			std::cin >> Ligne;
			std::cout << "Colonne ?";
			std::cin >> Colonne;
			if (Ligne >= 1 && Ligne <= 3 && Colonne >= 1 && Colonne <= 3)
				Saisie = true;
			else
				std::cout << "Erreur lors de la saisie, le nombre doit etre entre 1 et 3" << std::endl;
		} while (Saisie==false);
		Colonne = Colonne - 1;
		Ligne = Ligne - 1;
		if (GrilleMorpion[Ligne][Colonne] != '-')
		{
			std::cout << "L'emplacement a deja ete pris !" << std::endl;
			Remplis = false;
		}
		else
		{
			GrilleMorpion[Ligne][Colonne] = P.PionDuJoueur;
			Remplis = true;
		}
	} while (Remplis == false);
}


bool Morpion::Victoire()
{
	bool VictoryResult=false;
	int Retry = 0;
	int Compteur = 0;
	//Egalit�
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (GrilleMorpion[i][j] != '-')
				Compteur++;
		}
		if (Compteur == 9)
		{
			std::cout << "Egalite" << std::endl;
			std::cout << "Voulez-vous recommencer ? (1 Pour recommencer | 2 Pour quitter)" << std::endl;
			std::cin >> Retry;
			if (Retry == 1)
			{
				Morpion::Initialisation();
				return false;
			}
			else 
				return true;
		}
	}

	//Victoire
	for (int i = 0; i < 3; i++)
	{
		if (GrilleMorpion[i][0] == GrilleMorpion[i][1] && GrilleMorpion[i][1] == GrilleMorpion[i][2] && GrilleMorpion[i][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (GrilleMorpion[0][i] == GrilleMorpion[1][i] && GrilleMorpion[1][i] == GrilleMorpion[2][i] && GrilleMorpion[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (GrilleMorpion[0][0] == GrilleMorpion[1][1] && GrilleMorpion[1][1] == GrilleMorpion[2][2] && GrilleMorpion[0][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else if (GrilleMorpion[2][0] == GrilleMorpion[1][1] && GrilleMorpion[1][1] == GrilleMorpion[0][2] && GrilleMorpion[2][0] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			return true;
		}
		else
			VictoryResult = false;
	}
	return VictoryResult;
}