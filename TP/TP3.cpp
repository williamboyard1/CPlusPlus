#include <iostream>
#include <windows.h>
#include "Player.h"
#include "Morpion.h"
#include "Puissance4.h"

int main()
{
    //Creation des joueurs
    const Player Player1{ 'X',"Joueur 1" };
    const Player Player2{ 'O',"Joueur 2" };

    bool EnregistrementVictory = false;
    Morpion MorpionGame;
    Puissance4 Puissance4Game;
    MorpionGame.Initialisation();
    Puissance4Game.Initialisation();

    int MenuChoix = 0;

    do
    {
        std::cout << "MENU :" << std::endl;
        std::cout << "1 - Morpion " << std::endl;
        std::cout << "2 - Puissance 4" << std::endl;
        std::cin >> MenuChoix;
        //Morpion
        if (MenuChoix == 1)
        {
            system("CLS");
            std::cout << "=======================================" << std::endl;
            std::cout << "Morpion : " << std::endl;
            std::cout << "=======================================" << std::endl;
            do
            {
                MorpionGame.Afficher();
                std::cout << std::endl <<"Tour du joueur : " << Player1.NomDuJoueur << "(" << Player1.PionDuJoueur << ")" << std::endl;
                MorpionGame.Remplir(Player1);
                MorpionGame.Afficher();
                EnregistrementVictory = MorpionGame.Victoire();
                if (EnregistrementVictory == false)
                {
                    std::cout << std::endl << "Tour du joueur : " << Player2.NomDuJoueur << "(" << Player2.PionDuJoueur << ")" << std::endl;
                    MorpionGame.Remplir(Player2);
                    EnregistrementVictory = MorpionGame.Victoire();
                }
            } while (EnregistrementVictory == false);
            Sleep(2000);
            system("CLS");
        }
        //Puissance 4
        else if (MenuChoix == 2)
        {
            system("CLS");
            std::cout << "=======================================" << std::endl;
            std::cout << "Puissance 4 : " << std::endl;
            std::cout << "=======================================" << std::endl;
            do {
                Puissance4Game.Afficher();
                std::cout << std::endl << "Tour du joueur : " << Player1.NomDuJoueur << "(" << Player1.PionDuJoueur << ")" << std::endl;
                Puissance4Game.Remplir(Player1);
                Puissance4Game.Afficher();
                EnregistrementVictory = Puissance4Game.Victoire();
                if (EnregistrementVictory == false)
                {
                    std::cout << std::endl << "Tour du joueur : " << Player2.NomDuJoueur << "(" << Player2.PionDuJoueur << ")" << std::endl;
                    Puissance4Game.Remplir(Player2);
                    EnregistrementVictory = Puissance4Game.Victoire();
                }
            } while (EnregistrementVictory == false);
            Sleep(2000);
            system("CLS");
        } 
    } while (true);
}
