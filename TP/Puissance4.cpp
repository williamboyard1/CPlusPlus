#include "Puissance4.h"

void Puissance4::Afficher()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			std::cout << GrillePuissance4[i][j];
		}
		std::cout << std::endl;
	}
}

void Puissance4::Remplir(Player P)
{
	bool Remplis = false;
	bool Saisie = false;
	int Colonne;
	do
	{
		do
		{
			std::cout << "Colonne ?";
			std::cin >> Colonne;
			if (Colonne >= 1 && Colonne <= 7)
				Saisie = true;
			else
				std::cout << "Erreur lors de la saisie, le nombre doit etre entre 1 et 7" << std::endl;
		} while (Saisie == false);
		Colonne = Colonne - 1;
		if (GrillePuissance4[0][Colonne] != '-')
		{
			std::cout << "Colonne complete !" << std::endl;
			Remplis = false;
		}
		else
		{
			for (int Ligne = 3; Ligne >= 0; Ligne--)
			{
				if (GrillePuissance4[Ligne][Colonne] == '-' && Remplis == false)
				{
					GrillePuissance4[Ligne][Colonne] = P.PionDuJoueur;
					Remplis = true;
				}
			}
		}
	} while (Remplis == false);
}

bool Puissance4::Victoire()
{
	int Retry;
	int Compteur = 0;
	//Egalit�
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			if (GrillePuissance4[i][j] != '-')
				Compteur++;
		}
		if (Compteur == 28)
		{
			std::cout << "Egalite" << std::endl;
			std::cout << "Voulez-vous recommencer ? (1 Pour recommencer | 2 Pour quitter)" << std::endl;
		}
	}
	//Victoire 
	bool Victoire = false;
	//Lignes
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (GrillePuissance4[i][j] == GrillePuissance4[i][j + 1] && GrillePuissance4[i][j] == GrillePuissance4[i][j + 2] && GrillePuissance4[i][j] == GrillePuissance4[i][j + 3] && GrillePuissance4[i][j] != '-') 
			{
				std::cout << "Victoire d'un Joueur ! " << std::endl;
				Victoire = true;
			}
		}
	}
	//Colonnes
	for (int i = 0; i < 7; i++)
	{
		if (GrillePuissance4[0][i] == GrillePuissance4[1][i] && GrillePuissance4[1][i] == GrillePuissance4[2][i] && GrillePuissance4[2][i] == GrillePuissance4[3][i] && GrillePuissance4[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			Victoire = true;
		}
	}

	//Diagonale
	for (int i=0; i < 4; i++)
	{
		//Du Haut vers le Bas
		if (GrillePuissance4[0][i] == GrillePuissance4[1][i+1] && GrillePuissance4[0][i] == GrillePuissance4[2][i+2] && GrillePuissance4[0][i] == GrillePuissance4[3][i+3] && GrillePuissance4[0][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			Victoire = true;
		}
		//Du Bas vers le Haut
		if (GrillePuissance4[3][i] == GrillePuissance4[2][i + 1] && GrillePuissance4[3][i] == GrillePuissance4[1][i + 2] && GrillePuissance4[3][i] == GrillePuissance4[0][i + 3] && GrillePuissance4[3][i] != '-')
		{
			std::cout << "Victoire d'un Joueur ! " << std::endl;
			Victoire = true;
		}
	}
	return Victoire;
}

void Puissance4::Initialisation()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			GrillePuissance4[i][j] = '-';
		}
	}
}
