#pragma once
#include <iostream>
#include "Player.h"
class Puissance4
{
public:
	void Afficher();
	void Remplir(Player P);
	bool Victoire();
	void Initialisation();
private:
	char GrillePuissance4[4][7];
};